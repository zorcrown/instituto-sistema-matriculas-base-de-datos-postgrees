package Interface;

import java.awt.BorderLayout;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import Interface_Busquedas.*;

import javax.swing.ImageIcon;
import javax.swing.JDesktopPane;
import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;


import MatriculaAccidentesViales.Conexion;
import MatriculaAccidentesViales.Empresa;
import MatriculaAccidentesViales.Facultad;
import java.awt.SystemColor;

public class FrameMenuPrincipal extends JFrame {

	private JDesktopPane    escritorio;


	private	JMenuBar		barra_menus;
	private JInternalFrame  resultado_estudiante = new JInternalFrame();
	private JMenu 			menu_archivo;
	private JMenu 			menu_registro;
	private JMenu 			menu_matricula;
	private JMenu 			menu_editar;//---------new
	private JMenuItem 		editar_buscar_estudiante;//---------new
	private JMenuItem 		editar_buscar_materia;//---------new
	private JMenuItem 		editar_buscar_programa;//---------new
	private JMenuItem 		editar_buscar_facultad;//---------new
	private JMenuItem 		editar_buscar_profesor;//---------new
	private JMenuItem 		archivo_guardar;
	private JMenuItem 		archivo_salir;
	private JMenuItem 		registro_estudiante;
	private JMenuItem 		registro_facultad;
	private JMenuItem 		registro_programa;
	private JMenuItem 		registro_materia;
	private JMenuItem 		matricula_materia;
	private JLabel		fondo;
	private Empresa 		empresa;
	private Conexion   conexion = new Conexion();
	

	private IFrameRegistrarEstudiante 	iFrameRegistroEstudiante;
	private IFrameMatriculaModulos		iFrameMatriculaModulos;
	private IFrameRegistrarPrograma		iFrameRegistrarPrograma;
	private IFrameRegistrarFacultad		iFrameRegistrarFacultad;
	
	
	private IFrameRegistrarModulo		iFrameRegistrarModulo;
	private FrameMenuPrincipal			mainMenu = this;
	private Login_Estudiante			loginEstudiante;
	
	private IFrameBuscarEstudiante      buscar_estudiante;
	private IFrameBuscarProfesor		buscar_profesor;
	private IFrameBuscarMateria			buscar_materia;
	private IFrameBuscarFacultad		buscar_facultad;
	private IFrameBuscarPrograma		buscar_programa;
	
	Connection connection = null ;

	/**
	 * Create the frame.
	 */
	public FrameMenuPrincipal() {
		
		System.out.println(conexion.getUrl()+conexion.getUser()+conexion.getPw());
		try {
			
			connection = DriverManager.getConnection(conexion.getUrl(),conexion.getUser(),conexion.getPw());
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		if(connection != null)
		{
			JOptionPane.showMessageDialog(null, "Conexion Establecida");
		}
		else
		{
			JOptionPane.showMessageDialog(null, "Error al conectra con la base de datos");
		}
		
		empresa = new Empresa("instituto","calle 50");
		setSize(1280,780);
		setTitle("Menu Principal");
		setupwidgets();
		setupEvents();
	}

	private void setupwidgets() {
		
		escritorio = new JDesktopPane();
		escritorio.setBackground(SystemColor.inactiveCaption);
		escritorio.setLayout(null);
		getContentPane().add(escritorio);
		
		iFrameRegistroEstudiante 	= new IFrameRegistrarEstudiante(empresa,mainMenu);
		iFrameRegistrarPrograma 	= new IFrameRegistrarPrograma(empresa,mainMenu);
		iFrameRegistrarFacultad 	= new IFrameRegistrarFacultad(empresa,mainMenu);	
		iFrameRegistrarModulo		= new IFrameRegistrarModulo(empresa);
		loginEstudiante   			= new Login_Estudiante(iFrameMatriculaModulos, escritorio,empresa);		
		buscar_estudiante 			= new IFrameBuscarEstudiante(empresa, mainMenu,escritorio);
		buscar_profesor 			= new IFrameBuscarProfesor(empresa, mainMenu);
		buscar_materia 				= new IFrameBuscarMateria(empresa, mainMenu);
		buscar_facultad 			= new IFrameBuscarFacultad(empresa, mainMenu);
		buscar_programa 			= new IFrameBuscarPrograma(empresa, mainMenu);
		
		fondo		= new JLabel();
		
		
		
		ImageIcon icon_fondo = new ImageIcon("images/fondo2.png");	
		Image conversion_fondo = icon_fondo.getImage();
		Image tamano1 = conversion_fondo.getScaledInstance(getWidth(), getHeight(), Image.SCALE_SMOOTH);
		ImageIcon icon_final_fondo = new ImageIcon(tamano1);
		
		fondo.setIcon(icon_final_fondo);
		
		escritorio.add(fondo);		fondo.setBounds(0, 0, getWidth(), getHeight());

		barra_menus = new JMenuBar();
		setJMenuBar(barra_menus);

		menu_archivo = new JMenu("Archivo");
		barra_menus.add(menu_archivo);

		archivo_guardar = new JMenuItem("Guardar");
		menu_archivo.add(archivo_guardar);

		archivo_salir = new JMenuItem("Salir");
		menu_archivo.add(archivo_salir);

		menu_registro = new JMenu("Registro");
		barra_menus.add(menu_registro);
		
		//------------------------------new-------------------------------
		menu_editar = new JMenu("Editar");
		barra_menus.add(menu_editar);
		
		editar_buscar_estudiante = new JMenuItem("Buscar Estudiante");
		menu_editar.add(editar_buscar_estudiante);
		
		editar_buscar_profesor= new JMenuItem("Buscar Profesor");
		menu_editar.add(editar_buscar_profesor);
		
		editar_buscar_materia = new JMenuItem("Buscar Materia");
		menu_editar.add(editar_buscar_materia);
		
		editar_buscar_programa = new JMenuItem("Buscar Programa");
		menu_editar.add(editar_buscar_programa);
		
		editar_buscar_facultad= new JMenuItem("Buscar Facultad");
		menu_editar.add(editar_buscar_facultad);
		
		
		
		//------------------------------new-------------------------------

		registro_estudiante = new JMenuItem("Registrar Estudiante");
		menu_registro.add(registro_estudiante);

		registro_facultad = new JMenuItem("Registrar Facultad");
		menu_registro.add(registro_facultad);

		registro_programa = new JMenuItem("Registras Programa");
		menu_registro.add(registro_programa);

		registro_materia = new JMenuItem("Registrar Materia");
		menu_registro.add(registro_materia);

		menu_matricula = new JMenu("Matricula");
		barra_menus.add(menu_matricula);

		matricula_materia = new JMenuItem("Matricular Materia");
		menu_matricula.add(matricula_materia);
		setVisible(true);
	}

	private void setupEvents() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		registro_estudiante.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (!iFrameRegistroEstudiante.isVisible()) {
					iFrameRegistroEstudiante.clean();
					iFrameRegistroEstudiante.cargarDatos();
					escritorio.add(iFrameRegistroEstudiante);
					iFrameRegistroEstudiante.show();
				}
			}
		});

		registro_programa.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (!iFrameRegistrarPrograma.isVisible()) {		
					iFrameRegistrarPrograma.clean();
					iFrameRegistrarPrograma.cargarFacultades();
					escritorio.add(iFrameRegistrarPrograma);
					iFrameRegistrarPrograma.show();
				}
			}
		});

		registro_facultad.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				if (!iFrameRegistrarFacultad.isVisible()) {		
					iFrameRegistrarFacultad.clean();			
					escritorio.add(iFrameRegistrarFacultad,BorderLayout.CENTER);
					iFrameRegistrarFacultad.show();
				}
			}
		});
		
		registro_materia.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				if (!iFrameRegistrarModulo.isShowing()) {		
					iFrameRegistrarModulo.clean();			
					escritorio.add(iFrameRegistrarModulo,BorderLayout.CENTER);
					iFrameRegistrarModulo.show();
				}
			}
		});

		matricula_materia.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (!loginEstudiante.isShowing()) {		
					loginEstudiante.clean();
					escritorio.add(loginEstudiante);	
					loginEstudiante.show();					
				}
			}
		});

		archivo_salir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				dispose();
			}
		});
		
		
		//---------------------------------------------new----------------------------
		editar_buscar_estudiante.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				if (!buscar_estudiante.isVisible()) {		
					buscar_estudiante.clean();
					escritorio.add(buscar_estudiante);	
					buscar_estudiante.show();					
				}
			}
		});
		
		editar_buscar_profesor.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				if (!buscar_profesor.isVisible()) {		
					buscar_profesor.clean();
					escritorio.add(buscar_profesor);	
					buscar_profesor.show();					
				}
			}
		});
		
		editar_buscar_materia.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (!buscar_materia.isVisible()) {		
					buscar_materia.clean();
					escritorio.add(buscar_materia);	
					buscar_materia.show();					
				}
			}
		});
		
		editar_buscar_facultad.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				if (!buscar_facultad.isVisible()) {		
					buscar_facultad.clean();
					escritorio.add(buscar_facultad);	
					buscar_facultad.show();					
				}
			}
		});
		editar_buscar_programa.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				if (!buscar_programa.isVisible()) {		
					buscar_programa.clean();
					escritorio.add(buscar_programa);	
					buscar_programa.show();					
				}				
			}
		});
		
		//---------------------------------------------new----------------------------
	}


	public void setEscritorio(JInternalFrame result) {
		this.resultado_estudiante = result;
	}
	
	
}
