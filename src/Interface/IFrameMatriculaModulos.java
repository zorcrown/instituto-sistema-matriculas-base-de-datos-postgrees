package Interface;

import java.awt.BorderLayout;
import java.awt.Font;

import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

import MatriculaAccidentesViales.Empresa;
import MatriculaAccidentesViales.Estudiante;

public class IFrameMatriculaModulos extends JInternalFrame {

	private JPanel 					contentPane;
	private PanelInformacion_MM		panelInformacion;
	private	PanelMatricular_MM		panelMatricular;	
	private FrameMenuPrincipal		mainMenu;
	private Empresa 				empresa;
	private Estudiante				estudiante;
	/**
	 * Create the frame.
	 */
	public IFrameMatriculaModulos(Empresa empresa,Estudiante estudiante) {
		this.estudiante = estudiante;
		this.empresa = empresa;
		this.mainMenu = mainMenu;
		setupWidgets();
		setVisible(false);
	}

	private void setupWidgets() {
	
		setClosable(true);
		setBounds(10, 10, 1200, 600);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout());
		setContentPane(contentPane);
		
		JLabel lblMatriculaDeModulos = new JLabel("MATRICULA DE MODULOS");
		lblMatriculaDeModulos.setFont(new Font("Tahoma", Font.BOLD, 20));
		lblMatriculaDeModulos.setHorizontalAlignment(SwingConstants.CENTER);
		contentPane.add(lblMatriculaDeModulos, BorderLayout.NORTH);
		

		panelInformacion 	= new PanelInformacion_MM(estudiante);
		contentPane.add(panelInformacion,BorderLayout.CENTER);
		
		panelMatricular		= new PanelMatricular_MM(panelInformacion,empresa);
		contentPane.add(panelMatricular, BorderLayout.EAST);		
	}

	public Estudiante getEstudiante() {
		return estudiante;
	}

	public void setEstudiante(Estudiante estudiante) {
		this.estudiante = estudiante;
	}
}
