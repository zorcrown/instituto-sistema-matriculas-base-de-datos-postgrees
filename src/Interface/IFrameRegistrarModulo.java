package Interface;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JRadioButtonMenuItem;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

import MatriculaAccidentesViales.Empresa;
import MatriculaAccidentesViales.Facultad;
import MatriculaAccidentesViales.Modulo;
import MatriculaAccidentesViales.Periodo;
import MatriculaAccidentesViales.Profesor;
import MatriculaAccidentesViales.Programa;

public class IFrameRegistrarModulo  extends JInternalFrame{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	//-------------------------------------------------------------------------------------------------------------
	//Attributes
	private JLabel					lb_info;
	private JLabel					lb_creditos;
	private JRadioButtonMenuItem	rb_a;
	private JRadioButtonMenuItem	rb_b;
	private JLabel					lb_calendario;
	private JPanel					panel_center;
	private JTextField				tf_nombre_materia;
	private JTextField				tf_creditos;
	private JComboBox<Object>       cb_profesor;
	private JComboBox<Object> 		cb_programas;
	private JButton 				bt_aceptar; 
	private Empresa 				empresa;
	private ButtonGroup				bg_periodo;
	
	//-------------------------------------------------------------------------------------------------------------
	//constructor
	public IFrameRegistrarModulo(Empresa empresa) {
		
		setClosable(true);
		this.empresa = empresa;
		setTitle("Registro de Materias");
		setupWidgets();
		setupEvents();	
		setVisible(false);
	}
	//-------------------------------------------------------------------------------------------------------------
	//Methods
	private void setupWidgets() {
		
	
		
		
		getContentPane().setLayout(null);
		getContentPane().setBackground(new Color(23,124,171));
		setBounds(100, 100,900,300);		
		lb_info = new JLabel("REGISTRO DE MATERIAS");
		lb_info.setBounds(242,0, 416, 35);
		lb_info.setFont(new Font("Dialog", Font.BOLD, 19));
		lb_info.setHorizontalAlignment(SwingConstants.CENTER);
		getContentPane().add(lb_info, BorderLayout.NORTH);
		
		panel_center = new JPanel();
		panel_center.setBounds(20,41,840,200);
		panel_center.setBackground(new Color(88,194,243));
		getContentPane().add(panel_center, BorderLayout.CENTER);
		panel_center.setLayout(null);
		
		tf_nombre_materia = new JTextField();
		tf_nombre_materia.setBounds(68, 38, 200, 38);
		panel_center.add(tf_nombre_materia);
		tf_nombre_materia.setColumns(10);
		TextPrompt nombre_materia = new TextPrompt("Ingrese el nombre de la Materia", tf_nombre_materia);
		
		cb_programas = new JComboBox();
		cb_programas.setBounds(361, 88, 273, 38);
		cb_programas.addItem("--Seleccione programa--");	
		cargar_programas();
		panel_center.add(cb_programas);
		
		cb_profesor = new JComboBox();		
		cb_profesor.setBounds(361, 38, 273, 38);
		cb_profesor.addItem("--Seleccione profesor--");
		cargar_profesores();
		panel_center.add(cb_profesor);
		
		lb_calendario = new JLabel("Calendario Academico");
		lb_calendario.setBounds(660, 10, 150, 38);
		panel_center.add(lb_calendario);
			
		
		rb_a = new JRadioButtonMenuItem("A");
		rb_a.setBounds(700, 50,40,20);
		panel_center.add(rb_a);
		
		
		rb_b = new JRadioButtonMenuItem("B");
		rb_b.setBounds(700, 80, 40,20);
		panel_center.add(rb_b);
		
		lb_creditos = new JLabel("Creditos:");
		lb_creditos.setBounds(68, 89, 80, 20);
		panel_center.add(lb_creditos);
		
		tf_creditos = new JTextField();
		tf_creditos.setBounds(140, 88, 128, 38);
		panel_center.add(tf_creditos);
		tf_creditos.setColumns(10);
		TextPrompt nombre_credito = new TextPrompt("0", tf_creditos);
			
		bt_aceptar = new JButton("Registrar");
		bt_aceptar.setBounds(700, 138, 121, 38);
		panel_center.add(bt_aceptar);		
		
		bg_periodo = new ButtonGroup();
		bg_periodo.add(rb_a);
		bg_periodo.add(rb_b);			
	}
	//-------------------------------------------------------------------------------------------------------------
	
	private void setupEvents() {
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		bt_aceptar.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				if (!isNumeric(tf_nombre_materia.getText()) && isNumeric(tf_creditos.getText())) {
					String name,creditos;
					name = validarCadena(tf_nombre_materia.getText());					
					creditos = validarCadena(tf_creditos.getText());					
					if(empresa.registrarModulo(new Modulo(name,Integer.parseInt(creditos)),
						new Periodo("A"),
						(Programa)cb_programas.getSelectedItem(),(Profesor)cb_profesor.getSelectedItem())) {  // Modulo(String nombre, String docente, String facultad, String programa, int creditos)
						JOptionPane.showMessageDialog(null,"Registro Exitoso");
						dispose();
					}
					else {
						JOptionPane.showMessageDialog(null,"Registro Fallido\\nverifique sus datos");
					}
				}
				else {
					JOptionPane.showMessageDialog(null,"Registro Fallido\nverifique sus datos");
				}
			}
		});
	}
	//-------------------------------------------------------------------------------------------------------------
	
	public boolean isNumeric(String cadena) {

        boolean resultado;
        try {
            Integer.parseInt(cadena);
            resultado = true;
        } 
        catch (NumberFormatException excepcion) {
            resultado = false;
        }
        return resultado;
    }
	//-------------------------------------------------------------------------------------------------------------
	
	public String validarCadena(String texto) {
        java.util.StringTokenizer tokens = new java.util.StringTokenizer(texto);
        texto = "";
        while(tokens.hasMoreTokens()){
            texto += " "+tokens.nextToken();
        }
        texto = texto.toString();
        texto = texto.trim();
        texto = texto.toLowerCase();
        return texto;
    }
	
	//-------------------------------------------------------------------------------------------------------------
	public void clean() {
		
	}
	//-------------------------------------------------------------------------------------------------------------
	private void cargar_profesores() {
		Connection connection = null;
		String nombre, apellido, id;
		String url = "jdbc:postgresql://localhost:5432/base_datos_institutos";
		try {
			connection = DriverManager.getConnection(url,"postgres","Zcrown@347");
			Statement stm = connection.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
				    ResultSet.CONCUR_READ_ONLY);
			ResultSet rs = stm.executeQuery("select * from Profesor");
			rs.first();		
			do {
				nombre = rs.getString("nombre_pro");
				apellido = rs.getString("apellido_pro");
				id = rs.getString("clave_identificacion_pro");
				cb_profesor.addItem(new Profesor(nombre, apellido,id));
			}while(rs.next());			
		} catch (SQLException e) {
			System.out.print("Error de lectura: no se pudo cargar los profesores");
		}
	}	
	//-------------------------------------------------------------------------------------------------------------
	private void cargar_programas() {
		Connection connection = null;
		String nombre, codigo;
		String url = "jdbc:postgresql://localhost:5432/base_datos_institutos";
		try {
			connection = DriverManager.getConnection(url,"postgres","Zcrown@347");
			Statement stm = connection.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
				    ResultSet.CONCUR_READ_ONLY);
			ResultSet rs = stm.executeQuery("select clave_codigo_pro,nombre_pro from Programa");
			rs.first();		
			do {
				codigo = rs.getString("clave_codigo_pro");				
				nombre = rs.getString("nombre_pro");				
				cb_programas.addItem(new Programa(nombre,codigo));
			}while(rs.next());			
		} catch (SQLException e) {
			e.printStackTrace();
			System.out.print("Error de lectura");
		}
	}
}
