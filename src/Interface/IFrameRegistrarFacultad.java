package Interface;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

import MatriculaAccidentesViales.Empresa;
import MatriculaAccidentesViales.Facultad;

public class IFrameRegistrarFacultad extends JInternalFrame{
	
	private JLabel			lb_info;
	private JPanel			panel_center;
	private JTextField		tf_nombre;
	private JTextField 		tf_year;
	private JButton 		bt_aceptar; 
	private Empresa 		empresa;
	private FrameMenuPrincipal	mainMenu;

	/**
	 * Create the frame.
	 */
	public IFrameRegistrarFacultad(Empresa empresa, FrameMenuPrincipal mainMenu) {
		this.mainMenu = mainMenu;
		setClosable(true);
		this.empresa = empresa;
		setTitle("Regitro Faultad");
		setupWidgets();
		setupEvents();				
	}
	
	private void setupWidgets() {
		getContentPane().setLayout(null);
		getContentPane().setBackground(new Color(23,124,171));
		setBounds(100, 100, 450, 300);		
		lb_info = new JLabel("REGISTRO DE FACULTAD");
		lb_info.setBounds(12,0, 416, 35);
		lb_info.setFont(new Font("Dialog", Font.BOLD, 19));
		lb_info.setHorizontalAlignment(SwingConstants.CENTER);
		getContentPane().add(lb_info, BorderLayout.NORTH);
		
		panel_center = new JPanel();
		panel_center.setBounds(20,41,408,213);
		panel_center.setBackground(new Color(88,194,243));
		getContentPane().add(panel_center, BorderLayout.CENTER);
		panel_center.setLayout(null);
		
		tf_nombre = new JTextField();
		tf_nombre.setBounds(68, 38, 273, 38);
		panel_center.add(tf_nombre);
		tf_nombre.setColumns(10);
		TextPrompt nombre = new TextPrompt("Ingrese el nombre de la facultad", tf_nombre);
		
		tf_year = new JTextField();
		tf_year.setBounds(68, 88, 273, 38);
		panel_center.add(tf_year);
		tf_year.setColumns(10);
		TextPrompt year = new TextPrompt("Ingrese el año de creacion", tf_year);
		
		bt_aceptar = new JButton("Registrar");
		bt_aceptar.setBounds(151, 138, 121, 38);
		panel_center.add(bt_aceptar);
	}
	
	private void setupEvents() {
		bt_aceptar.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				if (!isNumeric(tf_nombre.getText()) && isNumeric(validarCadena(tf_year.getText()))) {
					String name, year;
					name = validarCadena(tf_nombre.getText());
					year = validarCadena(tf_year.getText());
					int intyear = Integer.parseInt(year);
					if(empresa.registrar_facultad(new Facultad(name,intyear))) {
						JOptionPane.showMessageDialog(null,"Registro Exitoso");
						dispose();
					}
					else {
						JOptionPane.showMessageDialog(null,"Registro Fallido\\nverifique sus datos");
					}
				}
				else {
					JOptionPane.showMessageDialog(null,"Registro Fallido\nverifique sus datos");
				}
			}
		});
	}
	
	public boolean isNumeric(String cadena) {

        boolean resultado;
        try {
            Integer.parseInt(cadena);
            resultado = true;
        } 
        catch (NumberFormatException excepcion) {
            resultado = false;
        }
        return resultado;
    }
	
	public String validarCadena(String texto) {
        java.util.StringTokenizer tokens = new java.util.StringTokenizer(texto);
        texto = "";
        while(tokens.hasMoreTokens()){
            texto += " "+tokens.nextToken();
        }
        texto = texto.toString();
        texto = texto.trim();
        texto = texto.toLowerCase();
        return texto;
    }
	
	public void clean() {
		tf_nombre.setText("");
		tf_year.setText("");
	}
}
