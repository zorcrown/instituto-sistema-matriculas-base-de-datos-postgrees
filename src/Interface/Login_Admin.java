package Interface;

import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.security.MessageDigest;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Arrays;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import org.apache.commons.codec.binary.Base64;

import MatriculaAccidentesViales.Conexion;
import MatriculaAccidentesViales.Facultad;

public class Login_Admin  extends JDialog {
	
	//--------------------------------------------------
	//Attributes
	
	private JTextField		tf_user;
	private JPasswordField	pf_pass;
	private JButton			bt_cancel,bt_verify,bt_recuperar;
	private JLabel			lb_recuperar;
	private SystemLogin	mainFrame;
	private Conexion   conexion = new Conexion();
	private	String name_user ="";

	//--------------------------------------------------
	
	//Constructor
	
	public Login_Admin(SystemLogin f) {
		super(f,true);
		mainFrame = f;

		setTitle("Ingreso Adminisrador");
		setSize(320,200);
		setupWidgets();
		setupEvents();
		setVisible(true);
	}
	
	

	//--------------------------------------------------
	//	Methods
	private void setupWidgets()
	{
	
		tf_user			= new JTextField();
		TextPrompt user = new TextPrompt("Nombre de Usuario", tf_user);
		pf_pass			= new JPasswordField();
		TextPrompt pwd = new TextPrompt("Contrasena", pf_pass);
		bt_cancel		= new JButton("Cancelar");
		bt_verify		= new JButton("Entrar");
		lb_recuperar	= new JLabel("Ha olvidado su contrasena:?:");
		bt_recuperar	= new JButton();
		
		setLayout(null);  // null layout
		
		// configure new limits setBounds(x, y, width, height)
		
		ImageIcon icon_recuperar = new ImageIcon("images/recuperar.png");	
		Image conversion_recuperar = icon_recuperar.getImage();
		Image tamano = conversion_recuperar.getScaledInstance(70, 30, Image.SCALE_SMOOTH);
		ImageIcon icon_final_recuperar = new ImageIcon(tamano);
		
		

		add(tf_user);  	tf_user.setBounds(40, 20, 230, 20);
		add(pf_pass); 	pf_pass.setBounds(40,45, 230, 20);
		add(bt_cancel); bt_cancel.setBounds(20, 80, 130, 20);
		add(bt_verify); bt_verify.setBounds(170, 80, 130, 20);
		add(lb_recuperar);	lb_recuperar.setBounds(20, 120, 200, 20);
		
		bt_recuperar.setIcon(icon_final_recuperar);
		add(bt_recuperar);	bt_recuperar.setBounds(210, 120, 70,30);
		
		
	}
	
	private void setupEvents()
	{
		
		
		bt_verify.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				
				String auxpass = Encriptar(pf_pass.getText());						
				if (Users_Passwords(tf_user.getText(), auxpass)) {	
					dispose();
					JOptionPane.showMessageDialog(null, "Welcome : " + name_user);
					dispose();
					new FrameMenuPrincipal();
					mainFrame.dispose();
				}else{
					setTitle("Incorrect Icome");
				}
			}
		});
		
		bt_cancel.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				dispose();
				
			}
		});
	}
	
	private boolean Users_Passwords(String name, String pass){
		String nom="",pw="";
		Connection connection = null;
		try {
			connection = DriverManager.getConnection(conexion.getUrl(),conexion.getUser(),conexion.getPw());
			Statement stm = connection.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
				    ResultSet.CONCUR_READ_ONLY);
			ResultSet rs = stm.executeQuery("select * from usuario where passwd = '"+pass+"'");
			rs.first();
			nom = rs.getString("nombre_usuario");
			pw = rs.getString("passwd");
			
			name_user =nom;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		if (nom.equals(name) && pw.equals(pass)) {
			return true;
		}else {
			return false;
		}			
	}
	
	public static String Encriptar(String texto) {
		 
        String secretKey = "qualityinfosolutions"; //llave para encriptar datos
        String base64EncryptedString = "";
 
        try {
 
            MessageDigest md = MessageDigest.getInstance("MD5");
            byte[] digestOfPassword = md.digest(secretKey.getBytes("utf-8"));
            byte[] keyBytes = Arrays.copyOf(digestOfPassword, 24);
 
            SecretKey key = new SecretKeySpec(keyBytes, "DESede");
            Cipher cipher = Cipher.getInstance("DESede");
            cipher.init(Cipher.ENCRYPT_MODE, key);
 
            byte[] plainTextBytes = texto.getBytes("utf-8");
            byte[] buf = cipher.doFinal(plainTextBytes);
            byte[] base64Bytes = Base64.encodeBase64(buf);
            base64EncryptedString = new String(base64Bytes);
 
        } catch (Exception ex) {
        	System.out.println("Error al encriptar");
        	ex.printStackTrace();        	
        }
        return base64EncryptedString;
}

}
