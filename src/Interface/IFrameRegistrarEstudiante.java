package Interface;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;

import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import MatriculaAccidentesViales.Empresa;


public class IFrameRegistrarEstudiante extends JInternalFrame {

	private JPanel 				contentPane;
	private PanelRegistro_RE 	panelRegistro_RE;
	private JLabel 				lb_title;
	private Empresa 			empresa;
	private FrameMenuPrincipal	mainMenu;

	/**
	 * Create the frame.
	 */
	public IFrameRegistrarEstudiante(Empresa empresa, FrameMenuPrincipal mainMenu) {
		this.empresa = empresa;
		this.mainMenu = mainMenu;
		setTitle("Registro Estudiante");
		setClosable(true);
		setBounds(100, 100, 450, 500);
		setupWidgets();
	}
	
	private void setupWidgets() {
		contentPane = new JPanel();
		contentPane.setForeground(Color.WHITE);
		contentPane.setBackground(new Color(23,124,171));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		lb_title = new JLabel("REGISTRO ESTUDIANTE",JLabel.CENTER);
		lb_title.setForeground(new Color(0, 0, 51));
		lb_title.setFont(new Font("FreeSerif", Font.BOLD | Font.ITALIC, 24));
		contentPane.add(lb_title,BorderLayout.NORTH);
		
		panelRegistro_RE = new PanelRegistro_RE(empresa,this);
		contentPane.add(panelRegistro_RE);
	}
	
	public void clean() {
		panelRegistro_RE.clean();
	}
	
	public void cargarDatos() {
		panelRegistro_RE.cargarProgramas();
	}
}
