package Interface;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Calendar;
import java.util.GregorianCalendar;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableModel;

import MatriculaAccidentesViales.Empresa;
import MatriculaAccidentesViales.Estudiante;
import MatriculaAccidentesViales.Modulo;

public class PanelMatricular_MM extends JPanel {
	
	private JTable 				table,table2;
	private JLabel				lb_info1;
	private JLabel 				lb_info2;
	private	JScrollPane 		sp_scroll,sp_materias;
	private DefaultTableModel	tm_table1,tm_table2;
	private	JButton				btnMatricular;
	private PanelInformacion_MM info;
	private Empresa 			empresa;
	private String 				idmat;
	protected int cont = 1;
	/**
	 * Create the panel.
	 */
	public PanelMatricular_MM(PanelInformacion_MM info,Empresa empresa) {
		this.empresa = empresa;
		this.info = info;
		setPreferredSize(new Dimension(446, 500));
		setupWidgets();
		setupEvents();		
	}	

	private void setupWidgets() {
		
		setLayout(null);		
		
		lb_info1 = new JLabel("MODULOS QUE PUEDE MATRICULAR");
		lb_info1.setFont(new Font("Tahoma", Font.BOLD, 15));
		lb_info1.setHorizontalAlignment(SwingConstants.CENTER);
		lb_info1.setBounds(0, 12, 437, 23);
		add(lb_info1);
		
		lb_info2 = new JLabel("MODULOS MATRICULADOS");
		lb_info2.setFont(new Font("Tahoma", Font.BOLD, 15));
		lb_info2.setHorizontalAlignment(SwingConstants.CENTER);
		lb_info2.setBounds(0, 269, 437, 23);
		add(lb_info2);
		
		tm_table2 = new DefaultTableModel(new Object[] {"ID","MODULO","CREDITOS"},0) {
			@Override
			public boolean isCellEditable(int row, int column) {
				return true;
			}
		};
		tm_table1 = new DefaultTableModel(new Object[] {"Materias a matricular"},0) {
			@Override
			public boolean isCellEditable(int row, int column) {
				if (column == 1) {
					return true;
				}
				else{
					return false;
				}
			}
		};
		cargarInformacion(info.getPeriodo());
		
		table2 = new JTable(tm_table2);						
		table2.setEnabled(false);	
		table = new JTable(tm_table1);
		
		sp_scroll = new JScrollPane(table2);
		sp_scroll.setBounds(12, 304, 425, 184);		
		add(sp_scroll);
		
		btnMatricular = new JButton("MATRICULAR");
		btnMatricular.setBounds(294, 234, 124, 23);
		add(btnMatricular);
		
		sp_materias = new JScrollPane(table);
		sp_materias.setBounds(12, 47, 423, 174);
		add(sp_materias);		
	}
	
	private void setupEvents() {					
		
		btnMatricular.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				int row = table.getSelectedRow();				
				Modulo mod = (Modulo)table.getValueAt(row,0);						
				if (empresa.matricular_modulo(new Estudiante(info.getIdEs()), mod, idmat)) {
					tm_table2.addRow(new Object[] {cont,mod.getNombre(),mod.getCreditos()});
					cont +=1;	
					tm_table1.removeRow(row);
					JOptionPane.showMessageDialog(null,"HA MATRIUCALO "+mod );
				}
			}
		});
	}
	
	private void cargarInformacion(String periodo) {
		
		//DECLARACIONES DE VARIABLES
		//===========================================================================================
		boolean cargar;
		String codpro,clave_idmatricula = "",modulos[];
		Connection connection = null;
		String nombre,pro ="" ;
		Modulo mod ;
		int creditos;
		String url = "jdbc:postgresql://localhost:5432/base_datos_institutos";	
		modulos = new String[5];
		for (int i=0;i<5;i++) {
			modulos[i] = "";
		}
		//===========================================================================================
		try {		
			connection = DriverManager.getConnection(url,"postgres","Zcrown@347");
			Statement stm = connection.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
				    ResultSet.CONCUR_READ_ONLY);
			
			ResultSet rs = stm.executeQuery("select clave_idmatricula from matricula where identificacion_esm = '"+info.getIdEs()+"'");
			rs.first();
			clave_idmatricula = rs.getString("clave_idmatricula");
			this.idmat = clave_idmatricula;
		}catch (SQLException e) {	
			e.printStackTrace();
			if(verificarMatricula()) {
				JOptionPane.showMessageDialog(null,"la matricula ya existe accediendo");						
			}else {
				System.out.println("No existe aun la matricula");
				if(crearMatricula())
					JOptionPane.showMessageDialog(null, "Una nueva matricula ha sido creada");
				else {
					System.out.println("No fue posible crear la matricula");
				}	
			}					
		}
		//===========================================================================================		
		try {		
			Statement stm = connection.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
				    ResultSet.CONCUR_READ_ONLY);
			ResultSet rs = stm.executeQuery("select nombre_mod1 from matricula_modulo where id_matricula1 = '"+clave_idmatricula+"'");				
			rs.first();
			int cont = 0;
			do {			
				modulos[cont] = rs.getString("nombre_mod1");
				cont++;					
			}while(rs.next());			
		}catch (SQLException e) {
			System.out.println("No se econtraron modulos matriculados");
		}		
		//===========================================================================================
		try {
			Statement stm = connection.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
				    ResultSet.CONCUR_READ_ONLY);
			ResultSet rs = stm.executeQuery("select clave_codigo_pro from Programa inner join Estudiante on Programa.clave_codigo_pro = estudiante.codigo_pro1\r\n" + 
					"where clave_identificacion_es ='"+info.getIdEs()+"'");
			rs.first();
			pro = rs.getString("clave_codigo_pro");
		}catch (SQLException e) {
			System.out.println("No se econtraro el programa");
		}
		//===========================================================================================
		try {			
			Statement stm = connection.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
				    ResultSet.CONCUR_READ_ONLY);			
			ResultSet rs = stm.executeQuery("select * from modulo inner join periodo on periodo.clave_tipo =Modulo.clave_tipo2\r\n" + 
					"inner join Programa on programa.clave_codigo_pro = modulo.codigo_pro2\r\n" + 
					"where clave_tipo = '"+periodo+"' and clave_codigo_pro = '"+pro+"'");
			rs.first();
			do {
				cargar = true;
				nombre = rs.getString("clave_nombre_mo");
				creditos = rs.getInt("creditos_mo");
				mod = new Modulo(nombre,creditos);
				for (int i = 0; i<5 ; i++) {
					if(modulos[i].equals(nombre) ) {
						cargar = false;						
					}						
				}
				if (cargar)
					tm_table1.addRow(new Object[] {mod});
				else {
					tm_table2.addRow(new Object[] {cont,nombre,creditos});
					cont +=1;					
				}				
			}while(rs.next());		
		} catch (SQLException e) {
			System.out.println("Error de lectura: no fue posible cargar matereas");
			System.out.println();
			e.printStackTrace();
		}
	}
	
	private boolean crearMatricula() {
		Calendar date = new GregorianCalendar();
		Connection connection = null;
		String fecha ="";
		fecha += Integer.toString(date.get(Calendar.MONTH));
		fecha += "-"+Integer.toString(date.get(Calendar.DAY_OF_MONTH)+1);
		fecha += "-"+Integer.toString(date.get(Calendar.YEAR));
		if (this.idmat !=null) {
			System.out.print("la matricula ya existe cargando");			
			return false;
		}else {
			this.idmat = Integer.toString((int)(Math.random()*10)+100);
			String url = "jdbc:postgresql://localhost:5432/base_datos_institutos";
			try {
				connection = DriverManager.getConnection(url,"postgres","Zcrown@347");											
				String periodo = info.getPeriodo();
				Statement stm = connection.createStatement();				
				stm.executeUpdate("insert into Matricula values ('"+idmat+"',"
								+ "'"+info.getIdEs()+"','"+fecha+"',"
								+ "'"+periodo+"')");	
				return true;
			} catch (SQLException e) {
				System.out.print("no fue psosible crear matricula");
				System.out.println();
				e.printStackTrace();
				return false;
			}
		}
	}
	
	 private boolean verificarMatricula() {    	
			Connection connection = null;
			String url = "jdbc:postgresql://localhost:5432/base_datos_institutos";
			try {
				connection = DriverManager.getConnection(url,"postgres","admin");
				Statement stm = connection.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
					    ResultSet.CONCUR_READ_ONLY);			
				ResultSet rs = stm.executeQuery("select clave_idmatricula from Matricula where identificacion_esm = '"+info.getIdEs()+"'");
				rs.first();				
				if (!rs.getString("clave_idmatricula").equals(null)) {
					return true;
				}
				else {
					return false;
				}
			} catch (SQLException e) {
				return false;			
			}
	    }
}
