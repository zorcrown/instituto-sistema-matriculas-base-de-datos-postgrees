package Interface;

import java.awt.Font;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Calendar;
import java.util.GregorianCalendar;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import MatriculaAccidentesViales.Estudiante;

public class PanelInformacion_MM extends JPanel {
	
	private JLabel 			lb_textaddress;
	private Estudiante 		estudent;
	private JLabel 			lb_textid;
	private JLabel 			lb_textname; 
	private JLabel 			lb_textfacultad; 
	private JLabel 			lb_textprograma;
	private JLabel 			lb_textanio;
	private JLabel 			lb_textemail;
	private JLabel 			lb_identificacion;
	private JLabel 			lb_nombre;
	private JLabel 			lb_facultad;
	private JLabel 			lb_programa;
	private	JLabel 			lb_anio;
	private JLabel 			lb_e_mail;
	private JLabel 			lb_direccion;
	private JLabel 			lblNewLabel_13;
	private JLabel 			lblNewLabel_14;
	private JLabel 			lblPeriodo;
	private JLabel 			lb_periodo;
	/**
	 * Create the panel.
	 */
	public PanelInformacion_MM(Estudiante estudiante) {		
		this.estudent = estudiante;		
		setupWidgets();
	}
	
	public void setupWidgets() {		
		setLayout(null);		
		JLabel lblInformacionDelEstudiante = new JLabel("INFORMACION DEL ESTUDIANTE");
		lblInformacionDelEstudiante.setFont(new Font("Tahoma", Font.BOLD, 15));
		lblInformacionDelEstudiante.setHorizontalAlignment(SwingConstants.CENTER);
		lblInformacionDelEstudiante.setBounds(41, 11, 376, 14);
		add(lblInformacionDelEstudiante);
		
		lb_textid = new JLabel("IDENTIFICACION:");
		lb_textid.setBounds(62, 56, 142, 14);
		add(lb_textid);
		
		lb_textname = new JLabel("NOMBRE:");
		lb_textname.setBounds(62, 86, 102, 14);
		add(lb_textname);
		
		lb_textfacultad = new JLabel("FACULTAD:");
		lb_textfacultad.setBounds(62, 116, 102, 14);
		add(lb_textfacultad);
		
		lb_textprograma = new JLabel("PROGRAMA:");
		lb_textprograma.setBounds(62, 146, 102, 14);
		add(lb_textprograma);
		
		lb_textanio = new JLabel("ANIO:");
		lb_textanio.setBounds(62, 176, 102, 14);
		add(lb_textanio);
		
		lblPeriodo = new JLabel("PERIODO");
		lblPeriodo.setBounds(62, 206, 102, 15);
		add(lblPeriodo);		
		
		lb_textemail = new JLabel("E-MAIL");
		lb_textemail.setBounds(62, 236, 102, 14);
		add(lb_textemail);
		
		lb_textaddress = new JLabel("DIRECCION");
		lb_textaddress.setBounds(62, 266, 102, 14);
		add(lb_textaddress);
		
		String []adicional = cargarInfAdicional();
		
		lb_identificacion = new JLabel(estudent.getIdentificacion());
		lb_identificacion.setBounds(264, 56, 175, 14);
		add(lb_identificacion);
		
		lb_nombre = new JLabel(estudent.getNombre()+" "+estudent.getApellido());
		lb_nombre.setBounds(264, 86, 175, 14);
		add(lb_nombre);
		
		lb_facultad = new JLabel(adicional[0]);
		lb_facultad.setBounds(264, 116, 175, 14);
		add(lb_facultad);
		
		lb_programa = new JLabel(adicional[1]);
		lb_programa.setBounds(264, 146, 175, 14);
		add(lb_programa);
		
		lb_anio = new JLabel(adicional[2]);
		lb_anio.setBounds(264, 176, 175, 14);
		add(lb_anio);
		
		lb_periodo = new JLabel(adicional[3]);
		lb_periodo.setBounds(264, 206, 175, 15);
		add(lb_periodo);
		
		lb_e_mail = new JLabel(estudent.getE_mail());
		lb_e_mail.setBounds(264, 236, 175, 14);
		add(lb_e_mail);
		
		lb_direccion = new JLabel(estudent.getDireccion());
		lb_direccion.setBounds(264, 266, 175, 14);
		add(lb_direccion);
		
		lblNewLabel_13 = new JLabel("EN EL LADO DERECHO SELECCIONE EL MODULO");
		lblNewLabel_13.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel_13.setBounds(12, 308, 526, 14);
		add(lblNewLabel_13);
		
		lblNewLabel_14 = new JLabel("Y LUEGO HAGA CLICK EN MATRICULAR");
		lblNewLabel_14.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel_14.setBounds(12, 330, 526, 14);
		add(lblNewLabel_14);			
	}
	
	public String [] cargarInfAdicional() {
		String []info = new String[4];
		Calendar c2 = new GregorianCalendar();
		Connection connection = null;
		String url = "jdbc:postgresql://localhost:5432/base_datos_institutos";
		try {
			connection = DriverManager.getConnection(url,"postgres","Zcrown@347");
			Statement stm = connection.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
				    ResultSet.CONCUR_READ_ONLY);
			ResultSet rs = stm.executeQuery("select clave_nombre_fac,nombre_pro from Estudiante inner join " + 
					"Programa on Programa.clave_codigo_pro = Estudiante.codigo_pro1 " + 
					"inner join Facultad on Facultad.clave_nombre_fac = Programa.clave_nombre_fac1 " + 
					"where clave_identificacion_es ='"+estudent.getIdentificacion()+"'");
			rs.first();
			info[0] = rs.getString("clave_nombre_fac");
			info[1] = rs.getString("nombre_pro");			
			info[2] = Integer.toString(c2.get(Calendar.YEAR));
			if (c2.get(Calendar.MONTH)<6) {
				info[3] = "A";
			}
			else {
				info[3] = "B";
			}
			return info;
		} catch (SQLException e) {
			System.out.print("Error de lectura: no dr purfo cargar la informacion del estudiante al panel");
			return null;
		}		
	}	
	
	public String getPeriodo() {
		return lb_periodo.getText();
	}
	
	public String getPrograma() {
		return lb_programa.getText();
	}
	
	public String getIdEs() {
		return lb_identificacion.getText();
	}
}
