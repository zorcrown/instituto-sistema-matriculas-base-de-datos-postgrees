package Interface;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import MatriculaAccidentesViales.Empresa;
import MatriculaAccidentesViales.Estudiante;
import MatriculaAccidentesViales.Facultad;
import MatriculaAccidentesViales.Programa;

import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import java.awt.Font;

public class PanelRegistro_RE extends JPanel {
	private JTextField 	tf_nombre;
	private JTextField tf_apellidos;
	private JTextField 	tf_id;
	private JTextField 	tf_address;
	private JTextField 	tf_phone;
	private JTextField 	tf_email;
	private Empresa 	empresa;
	private JButton 	bt_aceptar;
	private JLabel 		lb_programa; 
	private JComboBox 	cb_programas;
	
	
	private IFrameRegistrarEstudiante iFrameRegistrarEstudiante;

	/**
	 * Create the panel.
	 */
	public PanelRegistro_RE(Empresa empresa,IFrameRegistrarEstudiante iFrameRegistrarEstudiante) {
		this.iFrameRegistrarEstudiante = iFrameRegistrarEstudiante;
		this.empresa = empresa;
		setupWidgets();
		setupEvents();
	}

	private void setupWidgets() {
		setLayout(null);
		setBackground(new Color(88,194,243));
		setForeground(Color.CYAN);
		
		tf_nombre = new JTextField();
		tf_nombre.setBounds(54, 37, 339, 34);
		add(tf_nombre);
		tf_nombre.setColumns(10);
		TextPrompt nombre = new TextPrompt("Nombres ", tf_nombre);
		
		tf_id = new JTextField();
		tf_id.setBounds(54, 129, 339, 34);
		add(tf_id);
		tf_id.setColumns(10);
		TextPrompt id = new TextPrompt("Identificacion", tf_id);
		
		tf_address = new JTextField();
		tf_address.setBounds(54, 176, 339, 34);
		add(tf_address);
		tf_address.setColumns(10);
		TextPrompt address = new TextPrompt("Direccion", tf_address);
		
		tf_phone = new JTextField();
		tf_phone.setBounds(54, 222, 339, 34);
		add(tf_phone);
		tf_phone.setColumns(10);
		TextPrompt phone = new TextPrompt("Telefono", tf_phone);
		
		tf_email = new JTextField();
		tf_email.setBounds(54, 268, 339, 34);
		add(tf_email);
		tf_email.setColumns(10);
		TextPrompt email = new TextPrompt("E-mail", tf_email);
		
		bt_aceptar = new JButton("Registrar");
		bt_aceptar.setBounds(176, 371, 105, 27);
		add(bt_aceptar);
		
		lb_programa = new JLabel("Programa");
		lb_programa.setHorizontalAlignment(SwingConstants.RIGHT);
		lb_programa.setFont(new Font("Dialog", Font.BOLD, 16));
		lb_programa.setBounds(54, 314, 82, 26);
		add(lb_programa);
		
		cb_programas = new JComboBox();
		cb_programas.setBounds(147, 315, 246, 26);
		add(cb_programas);
		
		tf_apellidos = new JTextField();
		tf_apellidos.setBounds(54, 83, 339, 34);
		add(tf_apellidos);
		tf_apellidos.setColumns(10);
		TextPrompt apellidos = new TextPrompt("Apellidos", tf_apellidos);	
	}
	
	private void setupEvents() {
		
		bt_aceptar.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				String name, id, address, phone,email,apellido;
				name = tf_nombre.getText();
				id = tf_id.getText();
				address = tf_address.getText();
				phone = tf_phone.getText();
				email = tf_email.getText();
				apellido = tf_apellidos.getText();
				if (isNumeric(id) && !isNumeric(name) && !isNumeric(address) && isNumeric(phone) && !isNumeric(email)) {
					Estudiante estudent = new Estudiante(name,apellido,id,address,phone,email);
					JOptionPane.showMessageDialog(null,"Registrando: "+
												 estudent.toString()+"\n"+									
												 cb_programas.getSelectedItem());
					if (empresa.registrar_estudiante(estudent, (Programa)cb_programas.getSelectedItem())){
						JOptionPane.showMessageDialog(null,"Registrpo exitoso");
					}
					else {
						JOptionPane.showMessageDialog(null, "Registro fallido");
					}
				}
				else{
					JOptionPane.showMessageDialog(null, "ERROR verifique sus datos");
				}
						
			}
		});
	}
	
	public boolean isNumeric(String cadena) {

        boolean resultado;
        try {
            Long.parseLong(cadena);
            resultado = true;
        } 
        catch (NumberFormatException excepcion) {
            resultado = false;
        }
        return resultado;
    }
	
	public String validarCadena(String texto) {
        java.util.StringTokenizer tokens = new java.util.StringTokenizer(texto);
        texto = "";
        while(tokens.hasMoreTokens()){
            texto += " "+tokens.nextToken();
        }
        texto = texto.toString();
        texto = texto.trim();
        texto = texto.toLowerCase();
        return texto;
    }
	
	public void cargarProgramas() {
		
		Connection connection = null;
		String url = "jdbc:postgresql://localhost:5432/base_datos_institutos";
		try {
			connection = DriverManager.getConnection(url,"postgres","Zcrown@347");
			Statement stm = connection.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
				    ResultSet.CONCUR_READ_ONLY);
			ResultSet rs = stm.executeQuery("select * from Programa");
			rs.first();
			do {
				Programa pro = new Programa(rs.getString("nombre_pro"),rs.getString("clave_codigo_pro")); 
				cb_programas.addItem(pro);
			}while(rs.next());			
		} catch (SQLException e) {
			System.out.print("Error de lectura: no se pudo cargar los programas");
		}
	}
	
	public void clean() {
		tf_nombre.setText("");
		tf_apellidos.setText("");
		tf_address.setText("");
		tf_id.setText("");
		tf_email.setText("");
		tf_phone.setText("");	
		try {
			cb_programas.removeAllItems();
		}
		catch (Exception e) {			
		}
						
	}
}
