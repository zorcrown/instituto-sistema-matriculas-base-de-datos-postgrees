/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Interface;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.JButton;
import javax.swing.JDesktopPane;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import MatriculaAccidentesViales.Empresa;
import MatriculaAccidentesViales.Estudiante;

/**
 *
 * @author Cris_Zorcrown
 */
public class Login_Estudiante extends JInternalFrame{
    private JLabel              	lb_info1,lb_info2;
    private JTextField          	tf_user;
    private JPasswordField      	pf_pass;
    private JButton             	bt_verify;    
    private IFrameMatriculaModulos	matriculaModulos;
    private JDesktopPane			escritorio;
    private Empresa 				empresa;
    
    public Login_Estudiante (IFrameMatriculaModulos matriculaModulos,JDesktopPane escritorio, Empresa empresa){ 	
    	this.empresa = empresa;
    	this.escritorio = escritorio;
    	this.matriculaModulos = matriculaModulos;
    	setClosable(true);
        setTitle("Ingreso Sistema");       
        setupWidgets();
        setupEvents();  
    }
    
    private void setupWidgets(){
    	
    	setBounds(200,200,347,150);
        lb_info1    = new JLabel("Identificacion");
        lb_info2    = new JLabel("Password");
        tf_user     = new JTextField();
        pf_pass     = new JPasswordField();
        bt_verify   = new JButton("Aceptar");
        
        getContentPane().setLayout(null);
        
        getContentPane().add(lb_info1);  lb_info1.setBounds(20,20,107,20);
      //  getContentPane().add(lb_info2);  lb_info2.setBounds(20,45,107,20);
        getContentPane().add(tf_user);   tf_user.setBounds(145,20,180,20);
       // getContentPane().add(pf_pass);   pf_pass.setBounds(145,45,180,20);
        getContentPane().add(bt_verify); bt_verify.setBounds(109,86,130,20);   
    }

    private void setupEvents() {      
        bt_verify.addActionListener(new ActionListener() {
        	Estudiante es ;
            @Override
            public void actionPerformed(ActionEvent e) {
                es = buscarEstudiante(tf_user.getText());             
                if (es != null ) { 
                	dispose();
                	matriculaModulos = new IFrameMatriculaModulos(empresa, es);                	              
                	escritorio.add(matriculaModulos);
                	matriculaModulos.show();
                }             
                else
                	setTitle("Error");
            }
        });
    }
    
    public Estudiante buscarEstudiante(String idEs) {
    	String id,nombre,apellido,direccion,telefono,email;
		Connection connection = null;
		String url = "jdbc:postgresql://localhost:5432/base_datos_institutos";
		try {
			connection = DriverManager.getConnection(url,"postgres","Zcrown@347");
			Statement stm = connection.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
				    ResultSet.CONCUR_READ_ONLY);
			ResultSet rs = stm.executeQuery("select * from Estudiante where clave_identificacion_es ='"+idEs+"'");
			rs.first();
			id 			= rs.getString("clave_identificacion_es");
			nombre 		= rs.getString("nombre_es");
			apellido	= rs.getString("apellido_es");
			direccion 	= rs.getString("direccion_es");
			telefono 	= rs.getString("telefono_es");
			email 		= rs.getString("email_es");
			return new Estudiante(nombre,apellido,id,direccion,telefono,email);
		} catch (SQLException e) {
			//e.printStackTrace();
			return null;
		}
    }
    
    public void clean() {
    	tf_user.setText("");
    	pf_pass.setText("");
    }
}
