package Interface;



import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import org.postgresql.core.SetupQueryRunner;


import MatriculaAccidentesViales.Empresa;

import javax.swing.JInternalFrame;
import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import MatriculaAccidentesViales.Conexion ;

import javax.swing.JTabbedPane;

public class SystemLogin  extends JFrame{
	
	private JLabel		lb_tittle;
	private JLabel		lb_leyend;
	private JLabel		lb_info;
	private JLabel		lb_info2;
	private JLabel		fondo;
	private JButton		bt_admin;

	private Conexion   conexion = new Conexion();
	
	private SystemLogin	mainFrame = this;  
	private IFrameMatriculaModulos	matriculaModulos;
	private Empresa 				empresa;
	private String etiqueta ="";

	Connection connection = null ;
	
	public SystemLogin() {
		try {			
			connection = DriverManager.getConnection(conexion.getUrl(),conexion.getUser(),conexion.getPw());
		} catch (SQLException e) {
			System.out.println("No se conecto a la base de datos");
			e.printStackTrace();
		}		
		setTitle("Login System");
		setSize(500,600);
		setupWidgets();
		setupEvents();
		setVisible(true);
	}

	private void setupWidgets() {
		
		setLayout(null);
		
		lb_tittle		= new JLabel("Sistema Universitario",JLabel.CENTER);
		lb_leyend		= new JLabel("Create By: Systems Engineering Group::");
		lb_info			= new JLabel("Usuario:: Administrador del sistema de Matriculas ");
		lb_info2		= new JLabel();
		fondo			= new JLabel();
	
		bt_admin		= new JButton();

		lb_tittle.setFont(new Font("Rockwell Extra Bold", Font.PLAIN, 24));
		lb_info.setFont(new Font("Colonna MT", Font.PLAIN, 20));
		lb_info2.setFont(new Font("Berlin Sans FB", Font.PLAIN, 20));

		
		ImageIcon icon_admim = new ImageIcon("images/admin.png");	
		Image conversion_admin = icon_admim.getImage();
		Image tamano2 = conversion_admin.getScaledInstance(205, 205, Image.SCALE_SMOOTH);
		ImageIcon icon_final_admin = new ImageIcon(tamano2);
		
		ImageIcon icon_fondo = new ImageIcon("images/fondo.png");	
		Image conversion_fondo = icon_fondo.getImage();
		Image tamano1 = conversion_fondo.getScaledInstance(getWidth(), getHeight(), Image.SCALE_SMOOTH);
		ImageIcon icon_final_fondo = new ImageIcon(tamano1);
		

		bt_admin.setIcon(icon_final_admin);
		fondo.setIcon(icon_final_fondo);
		
		
		
		add(lb_info); lb_info.setBounds(30, 100, 450, 20);
		add(lb_tittle); lb_tittle.setBounds(30, 20, 400 ,20);
		add(bt_admin); bt_admin.setBounds(145, 160, 205, 205);
		add(lb_leyend); lb_leyend.setBounds(20, 530, 450 ,20);
		
		add(fondo); fondo.setBounds(0, 0, getWidth(), getHeight());

		
		
	}
	

	
	private void setupEvents() {
		
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		

		bt_admin.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				if (verificarUsuario()) {
					new Login_Admin(mainFrame);
				}
				else {
					JOptionPane.showMessageDialog(null, "creando usuario admin");
					new Create_Admin();
				}				
			}
		});
		
		bt_admin.addMouseListener(new MouseListener() {
			
			@Override
			public void mouseReleased(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mousePressed(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mouseExited(MouseEvent e) {
				ImageIcon icon_admim = new ImageIcon("images/admin.png");	
				Image conversion_admin = icon_admim.getImage();
				Image tamano2 = conversion_admin.getScaledInstance(205, 205, Image.SCALE_SMOOTH);
				ImageIcon icon_final_admin = new ImageIcon(tamano2);
				bt_admin.setIcon(icon_final_admin);
				add(bt_admin); bt_admin.setBounds(145, 160, 205, 205);
				etiqueta ="";
				lb_info2.setText(etiqueta);
				add(lb_info2); lb_info2.setBounds(30, 400, 440, 80);
				
				add(fondo); fondo.setBounds(0, 0, getWidth(), getHeight());
			}
			
			@Override
			public void mouseEntered(MouseEvent e) {
				
				ImageIcon icon_admim = new ImageIcon("images/admin.png");	
				Image conversion_admin = icon_admim.getImage();
				Image tamano2 = conversion_admin.getScaledInstance(215, 215, Image.SCALE_SMOOTH);
				ImageIcon icon_final_admin = new ImageIcon(tamano2);
				
				bt_admin.setIcon(icon_final_admin);
				bt_admin.setBounds(140, 155, 215, 215);
				etiqueta ="<html><p>Como administrador del sistema de matriculas, tiene control y acceso a todas las dependecias creadas.</p></html>";
				
				
				lb_info2.setText(etiqueta);
				add(lb_info2); lb_info2.setBounds(30, 400, 440, 80);
				add(fondo); fondo.setBounds(0, 0, getWidth(), getHeight());
				
			}
			
			@Override
			public void mouseClicked(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
		});
		
	}
	
	private boolean verificarUsuario() {

		String nom="",pw="";
		Connection connection = null;
		try {
			connection = DriverManager.getConnection(conexion.getUrl(),conexion.getUser(),conexion.getPw());
			Statement stm = connection.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
				    ResultSet.CONCUR_READ_ONLY);
			ResultSet rs = stm.executeQuery("select * from usuario where rol = 'ADMIN'");
			rs.first();
			if (rs.getString("rol")!=null) {
				return true;
			}
			else {
				return false;
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
		
	}
	
	public static void main(String[] args) {
		new SystemLogin();					
	}
	

}
