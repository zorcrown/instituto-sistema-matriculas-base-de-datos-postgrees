package Interface;

import java.awt.EventQueue;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import MatriculaAccidentesViales.Conexion;

import java.security.MessageDigest;
import java.util.Arrays;
import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import org.apache.commons.codec.binary.Base64;

public class Create_Admin extends JFrame {
	
	private JTextField		tf_username;
	private JPasswordField	pf_userpasswd;
	private JButton			bt_cancel,bt_verify;
	private SystemLogin		mainFrame;
	private Conexion   		conexion = new Conexion();
	private	String 			name_user ="";
	private JTextField 		tf_iduser;
	private JLabel 			lb_imgadmin;
	

	public Create_Admin() {
		//setClosable(true);
		setBounds(100, 100, 355, 300);		
		setupWidgets();
		setupEvents();
		setVisible(true);
	}
	
	private void setupWidgets(){
	
		tf_username					= new JTextField();
		TextPrompt tp_nameuser 	= new TextPrompt("Nombre de Usuario", tf_username);
		
		pf_userpasswd					= new JPasswordField();
		TextPrompt tp_passwd 	= new TextPrompt("Contraseña", pf_userpasswd);
		
		bt_cancel				= new JButton("Cancelar");
		bt_verify				= new JButton("Aceptar");
		
		tf_iduser = new JTextField();
		tf_iduser.setBounds(59, 147, 231, 21);
		TextPrompt tp_iduser 	= new TextPrompt("Identificacion", tf_iduser);
		getContentPane().add(tf_iduser);
		tf_iduser.setColumns(10);
		
		getContentPane().setLayout(null);  // null layout
		
		// configure new limits setBounds(x, y, width, height)
		
		ImageIcon icon_admin 	= new ImageIcon("images/admin.png");	
		Image conversion_admin 	= icon_admin.getImage();
		Image tamano = conversion_admin.getScaledInstance(98, 79, Image.SCALE_SMOOTH);
		icon_admin= new ImageIcon(tamano);
		
		getContentPane().add(tf_username);  	tf_username.setBounds(59, 115, 230, 20);
		getContentPane().add(pf_userpasswd); 	pf_userpasswd.setBounds(60,180, 230, 20);
		getContentPane().add(bt_cancel); bt_cancel.setBounds(24, 241, 130, 20);
		getContentPane().add(bt_verify); bt_verify.setBounds(200, 241, 130, 20);
	 
		lb_imgadmin = new JLabel("");
		lb_imgadmin.setBounds(129, 12, 98, 79);
		lb_imgadmin.setIcon(icon_admin);
		getContentPane().add(lb_imgadmin);
	}
	
	private void setupEvents(){			
		bt_verify.addActionListener(new ActionListener() {			
			@Override
			public void actionPerformed(ActionEvent e) {	
				Connection connection = null;
				String passww_enc = Encriptar(pf_userpasswd.getText());				
				try {
					connection = DriverManager.getConnection(conexion.getUrl(),conexion.getUser(),conexion.getPw());
					Statement stm = connection.createStatement();
					stm.executeUpdate("insert into usuario values ('"+tf_iduser.getText()+"','"+tf_username.getText()+"','ADMIN','"+passww_enc+"')");				
				} catch (SQLException e1) {
					e1.printStackTrace();
					return;
				}
				JOptionPane.showMessageDialog(null, "usuario agregado con exito");
				dispose();
			}
		});
		
		bt_cancel.addActionListener(new ActionListener() {			
			@Override
			public void actionPerformed(ActionEvent e) {
				dispose();				
			}
		});
	}
	
	public static String Encriptar(String texto) {
		 
        String secretKey = "qualityinfosolutions"; //llave para encriptar datos
        String base64EncryptedString = "";
 
        try {
 
            MessageDigest md = MessageDigest.getInstance("MD5");
            byte[] digestOfPassword = md.digest(secretKey.getBytes("utf-8"));
            byte[] keyBytes = Arrays.copyOf(digestOfPassword, 24);
 
            SecretKey key = new SecretKeySpec(keyBytes, "DESede");
            Cipher cipher = Cipher.getInstance("DESede");
            cipher.init(Cipher.ENCRYPT_MODE, key);
 
            byte[] plainTextBytes = texto.getBytes("utf-8");
            byte[] buf = cipher.doFinal(plainTextBytes);
            byte[] base64Bytes = Base64.encodeBase64(buf);
            base64EncryptedString = new String(base64Bytes);
 
        } catch (Exception ex) {
        	System.out.println("Error al encriptar");
        	ex.printStackTrace();        	
        }
        return base64EncryptedString;
}
}
