package Interface;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

import MatriculaAccidentesViales.Empresa;
import MatriculaAccidentesViales.Estudiante;
import MatriculaAccidentesViales.Facultad;
import MatriculaAccidentesViales.Programa;

public class IFrameRegistrarPrograma extends JInternalFrame {
	
	private JButton 		bt_aceptar;
	private JButton			bt_cancelar;
	private JPanel 			p_center;
	private Empresa			empresa;
	private JTextField 		tf_nombre;
	private JTextField 		tf_codigo;
	private JComboBox		cb_facultades;
	private JLabel			lb_info;
	private JLabel 			lb_info2;
	private JPanel			content;
	private FrameMenuPrincipal mainFrame;
	/**
	 * Create the frame.
	 */
	public IFrameRegistrarPrograma(Empresa empresa,FrameMenuPrincipal mainMenu) {
		this.mainFrame = mainMenu;
		setClosable(true);
		setTitle("Registrar Programa");
		this.empresa = empresa;
		setupWidgets();
		setupEvents();
	}

	private void setupWidgets() {
		
		setBounds(100, 100, 450, 375);
		content = new JPanel(null);
		content.setBackground(new Color(23,124,171));
		setContentPane(content);
		
		lb_info2 = new JLabel("REGISTAR PROGRAMA");
		lb_info2.setBounds(0, 0, 424, 29);
		content.add(lb_info2);
		lb_info2.setHorizontalAlignment(SwingConstants.CENTER);
		
		p_center = new JPanel();
		p_center.setBounds(10, 30, 300, 200);
		p_center.setBackground(new Color(88,194,243));
		p_center.setLayout(null);
		p_center.setBounds(10,30,420,300);
		content.add(p_center);
		
		tf_nombre = new JTextField();
		tf_nombre.setBounds(96, 138, 231, 39);
		p_center.add(tf_nombre);
		tf_nombre.setColumns(10);
		TextPrompt nombre = new TextPrompt("Ingrese el nombre del programa", tf_nombre);
		
		tf_codigo = new JTextField();
		tf_codigo.setBounds(96, 189, 231, 39);
		p_center.add(tf_codigo);
		tf_codigo.setColumns(10);
		TextPrompt codigo = new TextPrompt("Ingrese codigo de programa", tf_codigo);
		
		cb_facultades = new JComboBox();
		cb_facultades.setBounds(96, 77, 231, 39);
		p_center.add(cb_facultades);
		
		bt_aceptar = new JButton("Aceptar");
		bt_aceptar.setBounds(145,253,124,32);
		p_center.add(bt_aceptar);
		
		cargarFacultades();
		
		lb_info = new JLabel("Seleccione la facultad");
		lb_info.setFont(new Font("Dialog", Font.BOLD, 17));
		lb_info.setHorizontalAlignment(SwingConstants.CENTER);
		lb_info.setBounds(96, 42, 231, 32);
		p_center.add(lb_info);
		
	}

	private void setupEvents() {
		// TODO Auto-generated method stub
		bt_aceptar.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				if (!isNumeric(tf_nombre.getText())) {
					String name, codigo;
					name = validarCadena(tf_nombre.getText());
					codigo = validarCadena(tf_codigo.getText());
					String fac = (String)cb_facultades.getSelectedItem();
					if(empresa.registrar_programa(new Programa(name,codigo),new Facultad(fac))) {
						JOptionPane.showMessageDialog(null,"Registro Exitoso");
						dispose();
					}
					else {
						JOptionPane.showMessageDialog(null,"Registro Fallido\\nverifique sus datos");
					}
				}
				else {
					JOptionPane.showMessageDialog(null,"Registro Fallido\nverifique sus datos");
				}
			}
		});
	}
	
	public boolean isNumeric(String cadena) {

        boolean resultado;
        try {
            Integer.parseInt(cadena);
            resultado = true;
        } 
        catch (NumberFormatException excepcion) {
            resultado = false;
        }
        return resultado;
    }
	
	public String validarCadena(String texto) {
        java.util.StringTokenizer tokens = new java.util.StringTokenizer(texto);
        texto = "";
        while(tokens.hasMoreTokens()){
            texto += " "+tokens.nextToken();
        }
        texto = texto.toString();
        texto = texto.trim();
        texto = texto.toLowerCase();
        return texto;
    }
	
	public void cargarFacultades() {
		
		Connection connection = null;
		String url = "jdbc:postgresql://localhost:5432/base_datos_institutos";
		try {
			connection = DriverManager.getConnection(url,"postgres","Zcrown@347");
			Statement stm = connection.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
				    ResultSet.CONCUR_READ_ONLY);
			ResultSet rs = stm.executeQuery("select clave_nombre_fac from Facultad");
			rs.first();
			do {
				cb_facultades.addItem(rs.getString("clave_nombre_fac"));
			}while(rs.next());
			
		} catch (SQLException e) {
			System.out.print("Error de lectura");
		}
	}
	
	public void clean() {
		tf_nombre.setText("");
		tf_codigo.setText("");
		cb_facultades.removeAllItems();
	}
}
