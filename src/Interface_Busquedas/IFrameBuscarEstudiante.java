package Interface_Busquedas;

import java.awt.Color;
import java.awt.Font;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDesktopPane;
import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import Interface.*;
import MatriculaAccidentesViales.Conexion;
import MatriculaAccidentesViales.Empresa;
import MatriculaAccidentesViales.Facultad;
import MatriculaAccidentesViales.Programa;

public class IFrameBuscarEstudiante extends JInternalFrame {
	private JButton 				bt_aceptar;
	private JDesktopPane    		escritorio;
	private JButton					bt_cancelar;
	private JPanel 					p_center;
	private Empresa					empresa;
	private JTextField 				tf_id;
	private JLabel					lb_info;
	private JLabel 					lb_info2;
	private JPanel					content;
	private FrameMenuPrincipal 		mainFrame;
	private Conexion   				conexion = new Conexion();
	private IFrameBuscarEstudiante  buscar = this;
	private ResultadoEstudiante		resultadoEstudiante;
	
	private String name_user="";
	
	/**
	 * Create the frame.
	 */
	public IFrameBuscarEstudiante(Empresa empresa,FrameMenuPrincipal mainMenu, JDesktopPane escritorio) {
		this.mainFrame = mainMenu;
		this.escritorio = escritorio;
		setClosable(true);
		setTitle("Busqueda Indexada");
		this.empresa = empresa;
		setupWidgets();
		setupEvents();
		setVisible(false);
	}

	private void setupWidgets() {
		
		setBounds(100, 100, 450, 375);
		content = new JPanel(null);
		content.setBackground(new Color(144,150,151));
		setContentPane(content);
		
		lb_info2 = new JLabel("BUSCAR ESTUDIANTE");
		lb_info2.setBounds(0, 0, 424, 29);
		content.add(lb_info2);
		lb_info2.setHorizontalAlignment(SwingConstants.CENTER);
		
		p_center = new JPanel();
		p_center.setBounds(10, 30, 300, 200);
		p_center.setBackground(new Color(0,224,255));
		p_center.setLayout(null);
		p_center.setBounds(10,30,420,300);
		content.add(p_center);
		
		ImageIcon icon_estudiante = new ImageIcon("images/estudiante.png");	
		Image conversion_estudiante = icon_estudiante.getImage();
		Image tamano1 = conversion_estudiante.getScaledInstance(150, 150, Image.SCALE_SMOOTH);
		ImageIcon icon_final_esudiante = new ImageIcon(tamano1);
		
		lb_info = new JLabel("image");
		lb_info.setBounds(140, 20, 150,150);
		lb_info.setIcon(icon_final_esudiante);
		p_center.add(lb_info);
		
		
		
		tf_id = new JTextField();
		tf_id.setBounds(96, 190, 231, 30);
		p_center.add(tf_id);
		tf_id.setColumns(10);
		TextPrompt nombre = new TextPrompt("Identidicacion estudiante", tf_id);

		
		bt_aceptar = new JButton("Buscar");
		bt_aceptar.setBounds(150, 240, 100, 40);
		p_center.add(bt_aceptar);
	}

	private void setupEvents() {
		// TODO Auto-generated method stub
		bt_aceptar.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				if (isNumeric(tf_id.getText())) {
					
					
					String nom="",id="",ape="";
					Connection connection = null;
					try {
						connection = DriverManager.getConnection(conexion.getUrl(),conexion.getUser(),conexion.getPw());
						Statement stm = connection.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
							    ResultSet.CONCUR_READ_ONLY);
						ResultSet rs = stm.executeQuery("select * from Estudiante where clave_identificacion_es = '"+tf_id.getText()+"'");
						rs.first();
						nom = rs.getString("nombre_es");
						ape = rs.getString("apellido_es");
						id = rs.getString("clave_identificacion_es");
						
						name_user =nom;

					} catch (SQLException e) {
						e.printStackTrace();
						
					}
					
					if ( id.equals(tf_id.getText())) 
					{
						buscar.dispose();						
						JOptionPane.showMessageDialog(null, "Estudiante Encontrado \n\n"
								+ "Nombre: " + nom + " " + ape +"\n"
								+ "Identificacion: "+ id);								
						escritorio.add(resultadoEstudiante = new ResultadoEstudiante(tf_id.getText()));		
						resultadoEstudiante.show();
					}
					else{
						
					}													
				}
				else {
					JOptionPane.showMessageDialog(null,"Error, no es una identificacion valida");
				}
			}
		});
	}
	
	public boolean isNumeric(String cadena) {

        boolean resultado;
        try {
            Integer.parseInt(cadena);
            resultado = true;
        } 
        catch (NumberFormatException excepcion) {
            resultado = false;
        }
        return resultado;
    }
	

	
	
	public void clean() {
		tf_id.setText("");
	}
	
	

}
