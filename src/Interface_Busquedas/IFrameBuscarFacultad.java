package Interface_Busquedas;

import java.awt.Color;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import Interface.*;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

import MatriculaAccidentesViales.Conexion;
import MatriculaAccidentesViales.Empresa;
import MatriculaAccidentesViales.Programa;

public class IFrameBuscarFacultad extends JInternalFrame {
	private JButton 		bt_aceptar;
	private JButton			bt_cancelar;
	private JPanel 			p_center;
	private Empresa			empresa;
	private JTextField 		tf_nombre;
	private JLabel			lb_info;
	private JLabel 			lb_info2;
	private JPanel			content;
	private FrameMenuPrincipal mainFrame;
	private JComboBox<Object> 		cb_programas;
	private Conexion   conexion = new Conexion();
	

	/**
	 * Create the frame.
	 */
	public IFrameBuscarFacultad(Empresa empresa,FrameMenuPrincipal mainMenu) {
		this.mainFrame = mainMenu;
		setClosable(true);
		setTitle("Busqueda Indexada");
		this.empresa = empresa;
		setupWidgets();
		setupEvents();
		setVisible(false);
	}

	private void setupWidgets() {
		
		setBounds(100, 100, 450, 375);
		content = new JPanel(null);
		content.setBackground(new Color(144,150,151));
		setContentPane(content);
		
		lb_info2 = new JLabel("BUSCAR FACULTAD");
		lb_info2.setBounds(0, 0, 424, 29);
		content.add(lb_info2);
		lb_info2.setHorizontalAlignment(SwingConstants.CENTER);
		
		p_center = new JPanel();
		p_center.setBounds(10, 30, 300, 200);
		p_center.setBackground(new Color(0,224,255));
		p_center.setLayout(null);
		p_center.setBounds(10,30,420,300);
		content.add(p_center);
		
		ImageIcon icon_facultad = new ImageIcon("images/facultad.png");
		Image conversion_facultad = icon_facultad.getImage();
		Image tamano3 = conversion_facultad.getScaledInstance(150, 150, Image.SCALE_SMOOTH);
		ImageIcon icon_final_facultad = new ImageIcon(tamano3);
		
		lb_info = new JLabel("image");
		lb_info.setBounds(140, 20, 150,150);
		lb_info.setIcon(icon_final_facultad);
		p_center.add(lb_info);
		
		
		cb_programas = new JComboBox();
		cb_programas.setBounds(96, 190, 231, 30);
		cb_programas.addItem("--Seleccione programa--");	
		cargar_programas();
		p_center.add(cb_programas);
		
		
		bt_aceptar = new JButton("Buscar");
		bt_aceptar.setBounds(150, 240, 100, 40);
		p_center.add(bt_aceptar);
		
		
	}

	private void setupEvents() {
		
		cb_programas.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				
				JComboBox<Object> cb_current = (JComboBox<Object>)e.getSource();
				
				cb_current.getSelectedItem();
				
				
			}
		});
		
	}
	
	public boolean isNumeric(String cadena) {

        boolean resultado;
        try {
            Integer.parseInt(cadena);
            resultado = true;
        } 
        catch (NumberFormatException excepcion) {
            resultado = false;
        }
        return resultado;
    }
	
	public String validarCadena(String texto) {
        java.util.StringTokenizer tokens = new java.util.StringTokenizer(texto);
        texto = "";
        while(tokens.hasMoreTokens()){
            texto += " "+tokens.nextToken();
        }
        texto = texto.toString();
        texto = texto.trim();
        texto = texto.toLowerCase();
        return texto;
    }
	
	
	public void clean() {
		tf_nombre.setText("");
        cb_programas.removeAllItems();
		
	}
	private void cargar_programas() {
		Connection connection = null;
		String nombre, codigo;
		
		try {
			connection = DriverManager.getConnection(conexion.getUrl(),conexion.getUser(),conexion.getPw());
			Statement stm = connection.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
				    ResultSet.CONCUR_READ_ONLY);
			ResultSet rs = stm.executeQuery("select clave_codigo_pro,nombre_pro from Programa");
			rs.first();		
			do {
				codigo = rs.getString("clave_codigo_pro");				
				nombre = rs.getString("nombre_pro");				
				cb_programas.addItem(new Programa(nombre,codigo));
			}while(rs.next());			
		} catch (SQLException e) {
			e.printStackTrace();
			System.out.print("Error de lectura");
		}
	}
	
	

}
