package Interface_Busquedas;

import java.awt.Font;
import java.awt.Image;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDesktopPane;
import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

import MatriculaAccidentesViales.Conexion;
import MatriculaAccidentesViales.Empresa;
import MatriculaAccidentesViales.Estudiante;

public class ResultadoEstudiante  extends JInternalFrame{
	
	private JLabel lb_nombre,lb_apellido,lb_id,lb_address,lb_phone,lb_email,lbfacultad,lb_programa,lb_guardar,lb_salir,lb_editar,lb_semestre,lb_calendario;
	private JLabel lb_tittle1,lb_tittle2,lb_tittle3,lb_tittle4;
	private JTextField 	tf_nombre;
	private JTextField  tf_apellidos;
	private JTextField 	tf_id;
	private JTextField 	tf_address;
	private JTextField 	tf_phone;
	private JTextField 	tf_email;
	private JLabel 	lb_programa1;
	private JLabel 	lb_facultad1;
	private JLabel 	lb_sem;
	private JLabel 	lb_cal;
	private JLabel 	lb_foto;
	private JTable		tb_info;
	private JTable		tb_info_academica;
	private Conexion   conexion = new Conexion();
	
	private JButton		bt_guardar;
	private JButton		bt_editar;
	private JButton		bt_salir;
	
	private boolean cambios_realizados;
	

	private Empresa 	empresa;
	private Estudiante 		estudent = new Estudiante();
	
	private String id_estudiante="";
	


	public ResultadoEstudiante(String id) {
		
		this.id_estudiante = id;
		cambios_realizados = false;
		setClosable(true);
		setTitle("Resultado busqueda estudiante");
		setSize(900,700);
		setupWidgets();
		setupEvents();
		setVisible(false);
			
	}

	private void setupWidgets() {
		
		tf_address		= new JTextField();
		tf_apellidos	= new JTextField();
		tf_email		= new JTextField();
		tf_id			= new JTextField();
		tf_nombre		= new JTextField();
		tf_phone		= new JTextField();
		
		lb_programa1		= new JLabel();
		lb_facultad1		= new JLabel();
		lb_cal				= new JLabel();
		lb_sem				= new JLabel();
		lb_foto				= new JLabel("Foto",JLabel.CENTER);
		
		lb_address		= new JLabel("Direccion:",JLabel.RIGHT);
		lb_apellido		= new JLabel("Apellidos:",JLabel.RIGHT);
		lb_email		= new JLabel("E-Mail:",JLabel.RIGHT);
		lb_id			= new JLabel("Identificacion:",JLabel.RIGHT);
		lb_nombre		= new JLabel("Nombres:",JLabel.RIGHT);
		lb_phone		= new JLabel("Telefono:",JLabel.RIGHT);
		lb_programa		= new JLabel("Programa:",JLabel.RIGHT);
		lbfacultad		= new JLabel("Facultad:",JLabel.RIGHT);
		lb_semestre		= new JLabel("Semestre:",JLabel.RIGHT);
		lb_calendario	= new JLabel("Calendario:",JLabel.RIGHT);
		
		lb_editar		= new JLabel("Editar:",JLabel.RIGHT);
		lb_guardar		= new JLabel("Guardar",JLabel.RIGHT);
		lb_salir		= new JLabel("Salir",JLabel.RIGHT);
		
		lb_tittle1		= new JLabel("Informacinn Basica:",JLabel.CENTER);
		lb_tittle2		= new JLabel("Informacion Academica:",JLabel.CENTER);
		lb_tittle3		= new JLabel("Foto");
		lb_tittle4		= new JLabel();
		
		tb_info				= new JTable(new DefaultTableModel(0, 6));
		tb_info_academica 	= new JTable(new DefaultTableModel(0, 6));
		
		
		bt_editar		= new JButton();
		bt_guardar		= new JButton();
		bt_salir		= new JButton();
		
		
		lb_tittle1.setFont(new Font("Rockwell Extra Bold", Font.PLAIN, 18));
		lb_tittle2.setFont(new Font("Rockwell Extra Bold", Font.PLAIN, 18));
		
		ImageIcon icon_guardar = new ImageIcon("images/guardar.png");	
		Image conversion_guardar = icon_guardar.getImage();
		Image tamano1 = conversion_guardar.getScaledInstance(30,30, Image.SCALE_SMOOTH);
		ImageIcon icon_final_guardar = new ImageIcon(tamano1);
		
		ImageIcon icon_editar = new ImageIcon("images/editar.png");	
		Image conversion_editar = icon_editar.getImage();
		Image tamano2 = conversion_editar.getScaledInstance(30,30, Image.SCALE_SMOOTH);
		ImageIcon icon_final_editar = new ImageIcon(tamano2);
		
		ImageIcon icon_salir = new ImageIcon("images/salir.png");	
		Image conversion_salir = icon_salir.getImage();
		Image tamano3 = conversion_salir.getScaledInstance(30,30, Image.SCALE_SMOOTH);
		ImageIcon icon_final_salir = new ImageIcon(tamano3);
		
		ImageIcon icon_foto = new ImageIcon("images/foto.png");	
		Image conversion_foto = icon_foto.getImage();
		Image tamano4 = conversion_foto.getScaledInstance(150,200, Image.SCALE_SMOOTH);
		ImageIcon icon_final_foto = new ImageIcon(tamano4);
		
		bt_guardar.setIcon(icon_final_guardar);
		bt_editar.setIcon(icon_final_editar);
		bt_salir.setIcon(icon_final_salir);
		lb_foto.setIcon(icon_final_foto);
		
		
		setLayout(null);
		add(lb_foto);	lb_foto.setBounds(40, 20, 150, 200);
		
		add(lb_tittle1);		lb_tittle1.setBounds(220, 20, 300, 20);
		add(lb_nombre);			lb_nombre.setBounds(220, 60, 100, 20);				add(tf_nombre); 		tf_nombre.setBounds(325, 60, 150, 20);			
		add(lb_apellido); 		lb_apellido.setBounds(220, 85, 100, 20);				add(tf_apellidos); 		tf_apellidos.setBounds(325, 85, 150, 20);
		add(lb_id); 			lb_id.setBounds(220, 110, 100, 20);					add(tf_id); 			tf_id.setBounds(325, 110, 150, 20);
		add(lb_address); 		lb_address.setBounds(220, 135, 100, 20);				add(tf_address); 		tf_address.setBounds(325, 135, 150, 20);
		add(lb_phone); 			lb_phone.setBounds(220, 160, 100, 20);				add(tf_phone); 			tf_phone.setBounds(325, 160, 150, 20);
		add(lb_email); 			lb_email.setBounds(220, 185, 100, 20);				add(tf_email); 			tf_email.setBounds(325, 185, 150, 20);
		

		
		add(lb_guardar);		lb_guardar.setBounds(490, 70, 100, 30); 			add(bt_guardar);		bt_guardar.setBounds(600, 70, 30, 30);
		add(lb_editar);			lb_editar.setBounds(490, 110, 100, 30); 			add(bt_editar);			bt_editar.setBounds(600, 110, 30, 30);
		add(lb_salir);			lb_salir.setBounds(490, 150, 100, 30); 				add(bt_salir);			bt_salir.setBounds(600, 150, 30, 30);
		
		add(tb_info); tb_info.setBounds(20, 225, getWidth()-40,50);
		
		add(lb_tittle2); 		lb_tittle2.setBounds(20, 290, 300, 20);
		add(lbfacultad); 		lbfacultad.setBounds(20, 330, 100, 20);				add(lb_facultad1); 		lb_facultad1.setBounds(125, 330, 150, 20);
		add(lb_programa); 		lb_programa.setBounds(20, 355, 100, 20);			add(lb_programa1); 		lb_programa1.setBounds(125, 355, 150, 20);
		add(lb_semestre); 		lb_semestre.setBounds(20, 380, 100, 20);			add(lb_sem); 			lb_sem.setBounds(125, 380, 150, 20);
		add(lb_calendario); 	lb_calendario.setBounds(20, 405, 100, 20);			add(lb_cal); 			lb_cal.setBounds(125, 405, 150, 20);
		
		add(tb_info_academica); tb_info_academica.setBounds(20, 440, getWidth()-40,100);
		
	
		String[] strings = {"Nombre","Apellido","Identificacion","Direccion","Telefono","E-Mail"};
		
             ((DefaultTableModel) tb_info.getModel()).addRow(strings);
             
             llenartablainfo();
             
        String[] strings2 = {"#","id","Materia","Creditos","Semestre","Docente"};
     		
             ((DefaultTableModel) tb_info_academica.getModel()).addRow(strings2);
         
             
	}

	private void setupEvents() {
		
		
	}
	
	private void llenartablainfo()
	{
		
		String nom="",id="",ape="",dir="",tel="",email="";
		Connection connection = null;
		try {
			connection = DriverManager.getConnection(conexion.getUrl(),conexion.getUser(),conexion.getPw());
			Statement stm = connection.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
				    ResultSet.CONCUR_READ_ONLY);
			ResultSet rs = stm.executeQuery("select * from Estudiante where clave_identificacion_es = '"+id_estudiante+"'");
			rs.first();
			nom = rs.getString("nombre_es");
			ape = rs.getString("apellido_es");
			id = rs.getString("clave_identificacion_es");
			dir = rs.getString("direccion_es");
			tel = rs.getString("telefono_es");
			email = rs.getString("correo_es");
		

		} catch (SQLException e) {
			e.printStackTrace();
			
		}
		
		
		String[] strings = {nom,ape,id,dir,tel,email};
		 ((DefaultTableModel) tb_info.getModel()).addRow(strings);
		 
		 tf_nombre.setText(nom);
		 tf_apellidos.setText(ape);
		 tf_id.setText(id);
		 tf_address.setText(dir);
		 tf_phone.setText(tel);
		 tf_email.setText(email);
	}


}
