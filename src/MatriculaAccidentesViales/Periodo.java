package MatriculaAccidentesViales;

public class Periodo {
	private String tipo;
	
	public Periodo(String tipo) {
		this.tipo =tipo;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
		
}
