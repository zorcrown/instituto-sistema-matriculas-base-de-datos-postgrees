package MatriculaAccidentesViales;

public class Profesor {
	
	private String nombres;
	private String apellidos;
	private String identificacion;
	
	
	public Profesor(String nombres, String apellidos, String identificacion) {
		this.nombres = nombres;
		this.apellidos = apellidos;
		this.identificacion = identificacion;
	}
	
	@Override
	public boolean equals(Object otro) {
		if (otro.getClass() == this.getClass()) {
			Profesor otro2 = (Profesor) otro;
			return otro2.getIdentificacion().equals(this.getIdentificacion());
		}
		return false;
	}
	
	@Override
	public String toString() {
		return this.nombres;
	}

	public String getIdentificacion() {
		return identificacion;
	}

	public void setIdentificacion(String identificacion) {
		this.identificacion = identificacion;
	}

	public String getNombres() {
		return nombres;
	}

	public void setNombres(String nombres) {
		this.nombres = nombres;
	}

	public String getApellidos() {
		return apellidos;
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

}
